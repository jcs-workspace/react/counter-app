@echo off
:: ========================================================================
:: $File: run_react.bat $
:: $Date: 2020-05-16 14:33:25 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2020 by Shen, Jen-Chieh $
:: ========================================================================

cd ..

npm start
