/**
 * $File: navbar.jsx $
 * $Date: 2020-06-17 23:18:20 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2020 by Shen, Jen-Chieh $
 */

import React from 'react';
import ReactDOM from 'react-dom';

class Navbar extends React.Component {
  render () {
    console.log('NavBar - Rendered');

    return (
      <nav className="navbar navbar-light bg-light">
        <a className="navbar-brand" href="#">
          Navbar
          <span className="badge badge-pill badge-secondary">
            { this.props.totalCounters }
          </span>
        </a>
      </nav>
    );
  }
}

export default Navbar;
