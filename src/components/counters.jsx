/**
 * $File: counters.jsx $
 * $Date: 2020-05-28 21:24:22 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2020 by Shen, Jen-Chieh $
 */

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Counter from './counter';

class Counters extends Component {
  render () {
    console.log('Counters - Rendered');

    const { onReset, counters, onDelete, onIncrement } = this.props;

    return (
      <div>
        <button className="btn btn-primary btn-sm m-2"
                onClick={ onReset }>
          Reset
        </button>
        {
          counters.map(counter =>
            <Counter key={ counter.id }
                     onDelete= { onDelete }
                     onIncrement={ onIncrement }
                     counter={ counter }/>
          )
        }
      </div>
    );
  }
}

export default Counters;
