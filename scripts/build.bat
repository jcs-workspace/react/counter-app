@echo off
:: ========================================================================
:: $File: build.bat $
:: $Date: 2020-06-17 20:48:46 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2020 by Shen, Jen-Chieh $
:: ========================================================================

cd ..

npm run build
