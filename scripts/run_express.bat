@echo off
:: ========================================================================
:: $File: run_express.bat $
:: $Date: 2020-06-17 20:40:47 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2020 by Shen, Jen-Chieh $
:: ========================================================================

cd ..

node main.js
