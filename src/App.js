import React from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import NavBar from './components/navbar';
import Counters from "./components/counters";
import './App.css';

class App extends React.Component {
  state = {
    counters: [
      { id: 1, value: 4, },
      { id: 2, value: 0, },
      { id: 3, value: 0, },
      { id: 4, value: 0, },
    ],
  };

  constructor(props) {
    super(props);
    console.log('App - Contructor');
  }

  componentDidMount() {
    // AJAX call
    console.log('App - Mounted');
  }

  handleReset = () => {
    this.state.counters.map(c => {
      c.value = 0;
    });
    this.setState({ counters: this.state.counters });
  };

  handleIncrement = counter => {
    const index = this.state.counters.indexOf(counter);
    let tmpCounter = this.state.counters[index];
    ++tmpCounter.value;
    this.setState({ counters: this.state.counters });
  };

  handleDelete = (counterId) => {
    const counters = this.state.counters.filter(c => c.id !== counterId);
    this.setState({ counters: counters });
  };

  render() {
    console.log('App - Rendered');
    return (
      <React.Fragment>
        <NavBar totalCounters={ this.state.counters.filter(c => c.value > 0).length }/>
        <main className="container">
          <Counters counters={ this.state.counters }
                    onReset={ this.handleReset }
                    onIncrement={ this.handleIncrement }
                    onDelete={ this.handleDelete }/>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
